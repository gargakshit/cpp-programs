#include <iostream>

using namespace std;

int a;

int main() {
    int a;

    cout<<"Enter value for \"a\" of main(): ";
    cin>>a;

    cout<<"Enter value for \"a\" of global scope: ";
    cin>>::a;

    cout<<"\nValue are as follows\nLocal\tGlobal\n"
        <<a<<"\t"<<::a<<"\n";

    return 0;
}