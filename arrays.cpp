#include <iostream>

using namespace std;

int main() {
    int a[10];
    int sum;

    for (int i = 0; i < 10; i++) {
        cout<<"Enter value for "<<i+1<<"th place: ";
        cin>>a[i];
    }

    for (int i = 0; i < 10; i++) {
        sum += a[i];
    }

    cout<<"Sum is: "<<sum<<endl;
}