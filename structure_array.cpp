#include <iostream>
#include <stdio.h>

using namespace std;

struct student {
    int rollNo;

    int pMarks;
    int cMarks;
    int mMarks;

    int avgMarks;
};

int main() {

    student students[10];

    for (int i = 0; i < 10; i++) {

        cout<<"Student "<<i + 1<<"\n";

        students[i].rollNo = i + 1;

        cout<<"Physics Marks: ";
        cin>>students[i].pMarks;

        cout<<"Chemistry Marks: ";
        cin>>students[i].cMarks;

        cout<<"Maths Marks: ";
        cin>>students[i].mMarks;
    }

    for (int i = 0; i < 10; i++) {
        students[i].avgMarks = (students[i].pMarks + students[i].cMarks + students[i].mMarks ) / 100;
    }

    cout<<"Student Table\n\n";
    cout<<"RollNo\tPhysics\tChem\tMaths\tAvg\n";
    for (int i = 0; i < 10; i++) {
        cout<<students[i].rollNo<<"\t"<<students[i].pMarks<<"\t"
            <<students[i].cMarks<<"\t"<<students[i].mMarks<<"\t"
            <<students[i].avgMarks<<"\n";
    }

    return 0;
}