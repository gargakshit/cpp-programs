#include <iostream>

using namespace std;

int getAbsVal(int val) {
    return (val < 0 ? -val : val);
}

int main() {
    int val;
    cin>>val;
    cout<<getAbsVal(val);
}