#include <iostream>

using namespace std;

int main() {
    int a[3][3], row_sum[3], col_sum[3], d1_sum = 0, d2_sum = 0;

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            cin>>a[i][j];
        }
    }

    for (int i = 0; i < 3; i++) {
        row_sum[i] = 0;
        col_sum[i] = 0;
        for (int j = 0; j < 3; j++) {
            row_sum[i] += a[i][j];
            col_sum[i] += a[j][i];
        }
    }

    for (int i = 0; i < 3; i++) {
        d1_sum += a[i][i];
        d2_sum += a[2-i][i];
    }

    for (int i = 0; i < 3; i++) {
        cout<<"Row "<<i+1<<": Row Sum: "<<row_sum[i]
            <<" Col Sum: "<<col_sum[i]<<"\n";
    }

    cout<<"Sum for D1: "<<d1_sum
        <<" sum for D2: "<<d2_sum;

    return 0;
}