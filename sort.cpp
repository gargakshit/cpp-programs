#include <iostream>

using namespace std;

int main() {
    int a[10];

    for (int i = 0; i < 10; i++) {
        cout<<"Enter data for "<<i+1<<"th place: ";
        cin>>a[i];
    }

    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 9; j++) {
            if (a[j] > a[j+1]) {
                int temp;
                temp = a[j];
                a[j] = a[j+1];
                a[j+1] = temp;
            }
        }
    }

    for (int i = 0; i < 10; i++) {
        cout<<a[i]<<endl;
    }

    return 0;

}