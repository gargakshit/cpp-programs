#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

struct info {
    char name[20];
    int houseNo;
};

int main() {

    info one;

    cout<<"Enter name: ";
    gets(one.name);

    cout<<"Enter HouseNo: ";
    cin>>one.houseNo;

    cout<<"\nName: ";
    puts(one.name);
    cout<<"HouseNo: "<<one.houseNo<<"\n";

    return 0;
}