#include <iostream>

using namespace std;

int getGreater(int a, int b) {
    return (a > b ? a : b);
}

int main() {
    int a, b;
    cin>>a>>b;
    cout<<getGreater(a, b);

    return 0;
}