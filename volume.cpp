#include <iostream>

using namespace std;

int volume (int l, int b, int h) {
    return l*b*h;
}

int main() {
    int l, b, h;
    cout<<"Enter l, b, h\n";
    cin>>l>>b>>h;
    cout<<volume(l, b, h);
    return 0;
}