#include <iostream>
#include <stdio.h>

using namespace std;

int main() {
    char str[20];
    char ts;

    int found = 0, number = 0;

    gets(str);
    cin>>ts;

    for (int i = 0; str[i] != '\0'; i++) {
        if (str[i] == ts) {
            found = 1;
            number = i+1;
        }
    }

    if (found)
        cout<<"Found at: "<<number<<"\n";
    else
        cout<<"Not Found\n";
}