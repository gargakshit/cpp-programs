#include <iostream>

using namespace std;

void starline(int times, char ch) {
    for (int i = 0; i < times; i++)
        cout<<ch;
}

int main() {
    int times;
    char ch;

    cout<<"Enter number of times you want to print: ";
    cin>>times;

    cout<<"Enter character to print: ";
    cin>>ch;

    starline(times, ch);

    return 0;
}