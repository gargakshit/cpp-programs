#include <iostream>

using namespace std;

void swap(int &a, int &b) {
    int temp;

    temp = a;
    a = b;
    b = temp;
}

int main() {
    int a, b;

    cout<<"Enter numbers to swap:\n";
    cin>>a>>b;

    cout<<"Original Values are:\n"<<a<<"\t"<<b<<"\n";

    swap(a,b);

    cout<<"Swapped Values are: \n"<<a<<"\t"<<b<<"\n";

    return 0;
}