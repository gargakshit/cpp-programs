#include <iostream>

using namespace std;

char grade(int marks) {
    if (marks < 40) {
        return 'F';
    } else if (marks < 80) {
        return 'B';
    } else {
        return 'A';
    }
}

int main() {
    int marks;
    cin>>marks;

    cout<<grade(marks);
    return 0;
}