#include <iostream>

using namespace std;

int main() {
    int a[10], found = 0, pos = 0, toFind;;

    for (int i = 0; i < 10; i++) {
        cin>>a[i];
    }

    cout<<"Enter Number to Find: ";
    cin>>toFind;

    for (int i = 0; i < 10; i++) {
        if (a[i] == toFind) {
            found = 1;
            pos = i + 1;
            break;
        }
    }

    if (found) cout<<"Found at pos "<<pos<<"\n";
}