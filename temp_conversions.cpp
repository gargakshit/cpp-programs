#include <iostream>

using namespace std;

float convert(char to, float val) {
    float answer;

    if (to == 'c' || to == 'C') {
        answer = (val - 32.0)/1.8;
    } else if (to == 'f' || to == 'F') {
        answer = (val * 1.8) + 32.0;
    } else {
        val = 0;
    }

    return answer;
}

int main() {

    char to;
    float value;

    cout<<"Enter value to convert: ";
    cin>>value;
    cout<<"Press C to convert to C and F to convert to F: ";
    cin>>to;

    cout<<"Converted Value: "<<convert(to, value);

    return 0;
}