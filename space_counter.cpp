#include <iostream>
#include <stdio.h>

using namespace std;

int main() {
    char str[20];
    int count = 0;

    gets(str);

    for (int i = 0; str[i] != '\0'; i++) {
        if (str[i] == ' ')
            count++;
    }

    cout<<"Number of spaces in string is: "<<count;

    return 0;
}