#include <iostream>

using namespace std;

int pow(int, int);

int main() {

    int base, power;

    cin>>base>>power;

    cout<<pow(base, power)<<"\n";

    return 0;
}

int pow(int base, int power) {
    int ans = 1;
    for (int i = 0; i < power; i++)
        ans *= base;
    return ans;
}