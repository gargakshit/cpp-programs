#include <iostream>

using namespace std;

float si(float, float, float);

int main() {
    float p, r, t;

    cin>>p>>r>>t;
    cout<<si(p, r, t);
    return 0;
}

float si(float p, float r, float t) {
    return (p*r*t)/100;
}